FROM node:17-buster-slim

WORKDIR /usr/src/app

COPY rollup.config.js ./
COPY package*.json ./
COPY postcss.config.cjs ./
COPY tailwind.config.cjs ./

RUN npm install

COPY ./src ./src
COPY ./public ./public

RUN npm run build

EXPOSE 5000

ENV HOST=0.0.0.0
ENV BACKEND_URL="https://localhost:3000"

# Rebuild (set env variable) ans start
CMD [ "npm", "run", "update-start" ]
