const production = !process.env.ROLLUP_WATCH;
module.exports = {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {
      colors: {
        "ground": "#0B3954",
        "primary": "#E0FF4F",
        "secondary": "#FEFFFE",
        "success": "#E0FF4F",
        "failure": "#FF6663"
      }
    },
  },
  plugins: [],
  future: {
    purgeLayersByDefault: true,
    removeDeprecatedGapUtilities: true,
  },
}
