# Release Monitor Frontend

## How to run

```bash
IMAGE="registry.gitlab.com/gitlab-container-release-monitor/release-monitor-frontend/release:latest"
docker run -d \
  -e BACKEND_URL="https://releasemonitor.julianbuettner.dev/api" \
  --restart unless-stopped \
  --name release-monitor-frontend \
  -p 8080:8080 \
  "$IMAGE"
```
